﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProjectConsoleDB
{
    public abstract class CheckWord
    {
        public static bool checkINN(string value)
        {
            bool result=true;
            foreach (char s in value)
            {
                if (!("0123456789".Contains(s)))
                {
                    result=false;
                    break;
                }
            }
            return result & value.Length <= 10 & value.Length >= 8; 
        }
        public static bool checkDouble(string value)
        {
            double d=0;
            return double.TryParse(value, out d);
        }
        public static bool checkInt(string value)
        {
            int i = 0;
            return int.TryParse(value, out i);
        }
        public static bool checkString(string value)
        {
            return value.Length <= 20 && value.Length > 0;
        }
        public static bool checkDate(string value)
        {
            bool result = true;
            DateTime dt;
            int j;
            if (value.Length == 10)
            {
                for (int i = 0; i < value.Length; i++)
                {
                    if (i == 0 | i == 1 | i == 2 | i == 3 | i == 5 | i == 6 | i == 8 | i == 9)
                    {
                        if (!int.TryParse(value[i].ToString(), out j))
                        {
                            result = false;
                            break;
                        }
                    }
                    else if (!(value[i] == '-'))
                    {
                        result = false;
                        break;
                    }
                }
            }
            else
            {
                result = false;
            }
            return result & DateTime.TryParse(value, out dt);
        }
    }
}
