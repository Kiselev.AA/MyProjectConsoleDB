﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyProjectConsoleDB.DATA;
using MyProjectConsoleDB.DataBase;

namespace MyProjectConsoleDB
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<RentObject> RentObjects { get; set; }
        public DbSet<Rent> Rents { get; set; }
        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=RentDB;Username=postgres;Password=12345678");
            optionsBuilder.UseNpgsql(
                "Host=ec2-52-30-133-191.eu-west-1.compute.amazonaws.com;" +
                "Port=5432;" +
                "Database=d7qnfk76v75luk;" +
                "Username=rnqkullgepdqwj;" +
                "Password=e9a118237e391c662b53e82e313eaa561cce449da44639db1b9e36106bb2826a;" +
                "SSLMode=Prefer;" +
                "Trust Server Certificate=true");
        }
    }
}