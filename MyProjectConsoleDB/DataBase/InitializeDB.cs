﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyProjectConsoleDB.DATA;

namespace MyProjectConsoleDB.DataBase
{
    public static class InitializeDB
    {
        public static void StartInitDB()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                if (db.Contracts.Any())
                {
                    return;
                }
                List<Client> clients = new List<Client>();
                List<Contract> contracts = new List<Contract>();
                List<RentObject> rentObjects = new List<RentObject>();
                List<Rent> rents = new List<Rent>();

                clients.Add(new Client { Name = "Ромашка", Inn = "1111111111" });
                clients.Add(new Client { Name = "Лютик", Inn = "1212121212" });
                clients.Add(new Client { Name = "Газпром", Inn = "1231231234" });
                clients.Add(new Client { Name = "Сбербанк", Inn = "2121321321" });
                clients.Add(new Client { Name = "ВТБ", Inn = "4444111122" });
                clients.Add(new Client { Name = "Яндекс", Inn = "3334445566" });
                clients.Add(new Client { Name = "Мегафон", Inn = "8888889999" });
                clients.Add(new Client { Name = "Билайн", Inn = "1234123456" });
                clients.Add(new Client { Name = "Вайлдберис", Inn = "6666663333" });
                clients.Add(new Client { Name = "Ростелеком", Inn = "0980982323" });

                db.Clients.AddRange(clients);
                db.SaveChanges();

                rentObjects.Add(new RentObject { Name = "Склад 1", Square = 250 });
                rentObjects.Add(new RentObject { Name = "Склад 2", Square = 360 });
                rentObjects.Add(new RentObject { Name = "Склад 3", Square = 150 });
                rentObjects.Add(new RentObject { Name = "Склад 4", Square = 400 });
                rentObjects.Add(new RentObject { Name = "Склад 5", Square = 200 });
                rentObjects.Add(new RentObject { Name = "Офис 1", Square = 36.5 });
                rentObjects.Add(new RentObject { Name = "Офис 2", Square = 54.8 });
                rentObjects.Add(new RentObject { Name = "Офис 3", Square = 48.3 });
                rentObjects.Add(new RentObject { Name = "Офис 4", Square = 29.6 });
                rentObjects.Add(new RentObject { Name = "Офис 5", Square = 106.45 });

                db.RentObjects.AddRange(rentObjects);
                db.SaveChanges();

                contracts.Add(new Contract { Numb = "101", ClientId = 7, DateBegin = DateTime.Parse(s: "2021-01-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "102", ClientId = 6, DateBegin = DateTime.Parse(s: "2021-02-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "103", ClientId = 5, DateBegin = DateTime.Parse(s: "2021-03-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "104", ClientId = 4, DateBegin = DateTime.Parse(s: "2021-04-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "105", ClientId = 3, DateBegin = DateTime.Parse(s: "2021-05-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "106", ClientId = 2, DateBegin = DateTime.Parse(s: "2021-06-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "107", ClientId = 1, DateBegin = DateTime.Parse(s: "2021-07-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2022-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "108", ClientId = 10, DateBegin = DateTime.Parse(s: "2021-08-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2023-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "109", ClientId = 8, DateBegin = DateTime.Parse(s: "2021-09-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2023-12-31").ToUniversalTime() });
                contracts.Add(new Contract { Numb = "110", ClientId = 9, DateBegin = DateTime.Parse(s: "2021-10-01").ToUniversalTime(), DateEnd = DateTime.Parse(s: "2023-12-31").ToUniversalTime() });

                db.Contracts.AddRange(contracts);
                db.SaveChanges();

                rents.Add(new Rent { ContractId = 1, RentObjectID = 6, Square = 36.5, Price = 5000 });
                rents.Add(new Rent { ContractId = 2, RentObjectID = 7, Square = 54.8, Price = 5000 });
                rents.Add(new Rent { ContractId = 3, RentObjectID = 8, Square = 48, Price = 5000 });
                rents.Add(new Rent { ContractId = 4, RentObjectID = 9, Square = 29, Price = 5000 });
                rents.Add(new Rent { ContractId = 5, RentObjectID = 10, Square = 54, Price = 5000 });
                rents.Add(new Rent { ContractId = 6, RentObjectID = 5, Square = 200, Price = 3000 });
                rents.Add(new Rent { ContractId = 7, RentObjectID = 4, Square = 400, Price = 3000 });
                rents.Add(new Rent { ContractId = 8, RentObjectID = 3, Square = 150, Price = 3000 });
                rents.Add(new Rent { ContractId = 9, RentObjectID = 2, Square = 360, Price = 3000 });
                rents.Add(new Rent { ContractId = 10, RentObjectID = 1, Square = 150, Price = 3000 });

                db.Rents.AddRange(rents);
                db.SaveChanges();
            }
        }
    }
}
