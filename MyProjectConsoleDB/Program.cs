﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");
using System;
using System.Linq;
using MyProjectConsoleDB.DATA;
using MyProjectConsoleDB.DataBase;

namespace MyProjectConsoleDB
{
    class Program
    {
        static void Main(string[] args)
        {
            bool runTime = true;
            string commandlet = "";
            InitializeDB.StartInitDB();
            while (runTime)
            {
                Console.Write("Введите команду: ");
                commandlet = Console.ReadLine();
                runTime = Actions.Action(commandlet);
                Console.WriteLine();
            }
        }
     }
}