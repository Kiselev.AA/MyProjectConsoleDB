﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyProjectConsoleDB.DATA;

namespace MyProjectConsoleDB
{
    internal abstract class Actions
    {
        private List<string> entitys = new List<string>() { "client", "contract", "rentobject", "rent" };
        //private CheckWord Checker=new CheckWord();

        public static bool Action(string commandlet)
        {
            bool result = true;
            switch (commandlet.Split(' ')[0].Split('_')[0].ToUpper())
            {
                case "HELP":
                    HELP();
                    break;
                case "LS":
                    if (!LS(commandlet))
                    {
                        Console.WriteLine("Ошибка выполнения вывода таблицы!");
                    }
                    break;
                case "ADD":
                    if (!ADD(commandlet))
                    {
                        Console.WriteLine("Ошибка выполнения добавления записи! Изучите HELP!");
                    }
                    break;
                //case "CHANGE":
                //    if (!CHANGE(commandlet))
                //    {
                //        Console.WriteLine("Ошибка выполнения изменения записи!");
                //    }
                //    break;
                //case "DEL":
                //    if (!DEL(commandlet))
                //    {
                //        Console.WriteLine("Ошибка выполнения удаления записи!");
                //    }
                //    break;
                case "EXIT":
                    result = false;
                    break;
                default:
                    Console.WriteLine("Введенная команда не опознана! Можете ввести HELP для получения списка допкстимых команд");
                    break;
            }
            return result;
        }
        private static void HELP()
        {
            Console.WriteLine("Инструкция:");

            Console.WriteLine("HELP - вызов справки;");
            Console.WriteLine("LS nametable - вывести все строки таблицы;");

            Console.WriteLine("ADD_CLIENT name(строка до 20 знаков) inn(число от 8 до 10 знаков) - добавить клиента.");
            Console.WriteLine("ADD_RENTOBJECT name(строка до 20 знаков) squre(число с плавующей точкой) - добавить объект недвижимости.");
            Console.WriteLine("ADD_CONTRACT idclient(целочисленное число) numb(строка до 20 знаков) datebegin(гггг-мм-дд) dateend(гггг-мм-дд) - добавить договор арендны.");
            Console.WriteLine("ADD_RENT idcontract(целочисленное число) idobject(целочисленное число) squre(число с плавующей точкой) price(число с плавующей точкой) - добавить информацию по аренде помещения.");

            //Console.WriteLine("CHANGE_CLIENT id(целочисленное число) newname(строка до 20 знаков) newinn(число от 8 до 10 знаков) - добавить клиента.");
            //Console.WriteLine("CHANGE_OBJECT id(целочисленное число) newname(строка до 20 знаков) newsqure(число с плавующей точкой) - добавить объект недвижимости.");
            //Console.WriteLine("CHANGE_CONTRACT id(целочисленное число) newidclient(целочисленное число) newnumb(строка до 20 знаков) newdatebegin(дд.мм.гггг) newdateend(дд.мм.гггг) - добавить договор арендны.");
            //Console.WriteLine("CHANGE_RENT id(целочисленное число) newidcontract(целочисленное число) newidobject(целочисленное число) newsqure(число с плавующей точкой) newprice(число с плавующей точкой) - добавить информацию по аренде помещения.");


            Console.WriteLine("EXIT - выход из приложения.");
        }
        private static bool LS(string commandlet)
        {
            bool result=true;
            using (ApplicationContext db = new ApplicationContext())
            {
                if (commandlet.Split(' ').Length > 1)
                {
                    if (commandlet.Split(' ')[1].ToUpper() == "CLIENT")
                    {
                        var rows = db.Clients.ToList();
                        Console.WriteLine(commandlet.Split(' ')[1].ToUpper() + ":");
                        foreach (var row in rows)
                        {
                            Console.WriteLine(row.ToString());
                        }
                    }
                    else if (commandlet.Split(' ')[1].ToUpper() == "CONTRACT")
                    {
                        var rows = db.Contracts.ToList();
                        Console.WriteLine(commandlet.Split(' ')[1].ToUpper() + ":");
                        foreach (Contract row in rows)
                        {
                            Console.WriteLine(row.ToString());
                        }
                    }
                    else if (commandlet.Split(' ')[1].ToUpper() == "RENTOBJECT")
                    {
                        var rows = db.RentObjects.ToList();
                        Console.WriteLine(commandlet.Split(' ')[1].ToUpper() + ":");
                        foreach (RentObject row in rows)
                        {
                            Console.WriteLine(row.ToString());
                        }
                    }
                    else if (commandlet.Split(' ')[1].ToUpper() == "RENT")
                    {
                        var rows = db.Rents.ToList();
                        Console.WriteLine(commandlet.Split(' ')[1].ToUpper() + ":");
                        foreach (Rent row in rows)
                        {
                            Console.WriteLine(row.ToString());
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
        private static bool ADD(string commandlet)
        {
            bool result = true;
            using (ApplicationContext db = new ApplicationContext())
            {
                if (commandlet.Split(' ').Length > 1 && commandlet.Split(' ')[0].Split('_').Length > 1)
                {
                    if (commandlet.Split(' ')[0].Split('_')[1].ToUpper() == "CLIENT")
                    {
                        if (commandlet.Split(' ').Length == 3)
                        {
                            string name = commandlet.Split(' ')[1];
                            string inn = commandlet.Split(' ')[2];
                            if (CheckWord.checkString(name) &&
                                CheckWord.checkINN(inn))
                            {
                                Client row = new Client
                                {
                                    Name = name,
                                    Inn = inn
                                };
                                db.Clients.AddRange(row);
                                db.SaveChanges();
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else if (commandlet.Split(' ')[0].Split('_')[1].ToUpper() == "CONTRACT")
                    {
                        if (commandlet.Split(' ').Length == 5)
                        {
                            string clientid = commandlet.Split(' ')[1];
                            string numb = commandlet.Split(' ')[2];
                            string datebegin = commandlet.Split(' ')[3];
                            string dateend = commandlet.Split(' ')[4];
                            if (CheckWord.checkInt(clientid) &&
                                CheckWord.checkInt(numb) &&
                                CheckWord.checkDate(datebegin) &&
                                CheckWord.checkDate(dateend))
                            {
                                Contract row = new Contract
                                {
                                    ClientId = int.Parse(clientid),
                                    Numb = numb,
                                    DateBegin = DateTime.Parse(s: datebegin).ToUniversalTime(),
                                    DateEnd = DateTime.Parse(s: dateend).ToUniversalTime()
                                };
                                db.Contracts.AddRange(row);
                                db.SaveChanges();
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else if (commandlet.Split(' ')[0].Split('_')[1].ToUpper() == "RENTOBJECT")
                    {
                        if (commandlet.Split(' ').Length == 3)
                        {
                            string name = commandlet.Split(' ')[1];
                            string square = commandlet.Split(' ')[2];
                            if (CheckWord.checkString(name) &&
                                CheckWord.checkDouble(square))
                            {
                                RentObject row = new RentObject
                                {
                                    Name = name,
                                    Square = double.Parse(square)
                                };
                                db.RentObjects.AddRange(row);
                                db.SaveChanges();
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else if (commandlet.Split(' ')[0].Split('_')[1].ToUpper() == "RENT")
                    {
                        if (commandlet.Split(' ').Length == 5)
                        {
                            string contractid = commandlet.Split(' ')[1];
                            string objectid = commandlet.Split(' ')[2];
                            string square = commandlet.Split(' ')[3];
                            string price = commandlet.Split(' ')[4];
                            if (CheckWord.checkInt(contractid) &&
                                CheckWord.checkInt(objectid) &&
                                CheckWord.checkDouble(square) &&
                                CheckWord.checkDouble(price))
                            {
                                Rent row = new Rent
                                {
                                    ContractId = int.Parse(contractid),
                                    RentObjectID = int.Parse(objectid),
                                    Square = double.Parse(square),
                                    Price = double.Parse(price)
                                };
                                db.Rents.AddRange(row);
                                db.SaveChanges();
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
