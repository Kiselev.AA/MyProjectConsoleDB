﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProjectConsoleDB.DATA
{
    public class Contract
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public string Numb { get; set; }
        public DateTime DateBegin { get; set; }
        public DateTime DateEnd { get; set; }

        public override string ToString()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                return $"{Id}. {db.Clients.Find(ClientId).Name}({ClientId}) {Numb} {DateBegin} {DateEnd}";
            }
        }
    }
}
