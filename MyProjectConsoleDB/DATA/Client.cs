﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProjectConsoleDB.DATA
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Inn { get; set; }

        public override string ToString() => $"{Id}. {Name} {Inn}";
    }
}
