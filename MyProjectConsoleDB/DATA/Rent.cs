﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProjectConsoleDB.DATA
{
    public class Rent
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public Contract Contract { get; set; }
        public int RentObjectID { get; set; }
        public RentObject RentObject { get; set; }
        public double Square { get; set; }
        public double Price { get; set; }


        public override string ToString()
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                return $"{Id}. {db.Contracts.Find(ContractId).Numb} {db.RentObjects.Find(RentObjectID).Name} {Square} {Price}";
            }
        }
    }
}
